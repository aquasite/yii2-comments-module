<li class="comment" data-id="<?= $model->primaryKey ?>">
    <div class="text"><?= $model->text ?></div>
    <?php if ($type === \terrasoff\yii2\comments\Module::TYPE_NESTED) { ?>
        <div class="comment-reply">reply</div>
    <?php } ?>
    <div class="rating"><?= $model->rating ?></div>
</li>