<?php
use Yii;
use terrasoff\yii2\comments\assets\CommentsAsset;
CommentsAsset::register($this);
?>

<!-- Modal -->
<div class="modal fade" id="CommentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Leave a Comment</h4>
            </div>
            <div class="modal-body">
                <div class="well">
                    <blockquote class="comment">none</blockquote>
                    <form role="form">



                        <input type=hidden name="Comment[idParent]" value="0">
                        <input type=hidden name="Comment[className]" value="<?= $className ?>">
                        <input type=hidden name="Comment[idClass]" value="<?= $idClass ?>">
                        <?php if (Yii::$app->user->isGuest) { ?>
                            <div class="control-group">
                                <div class="controls">
                                    <input type="text" class="form-control" placeholder="Full Name" required
                                           data-validation-required-message="Please enter your name"
                                           name="Comment[username]" value="user" />
                                    <p class="help-block"></p>
                                </div>
                            </div>
                        <?php } else { ?>
                            <input type=hidden name="Comment[idUser]" value="<?= Yii::$app->user->id ?>">
                        <?php } ?>

                        <div class="form-group">
                            <div class="control-group">
                                <div class="controls">
                                    <textarea rows="5" cols="100" class="form-control"
                                              placeholder="Enter you comment"
                                              data-validation-required-message="Please enter your message"
                                              style="resize:none" required
                                              name="Comment[text]">comment</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <?= \yii\captcha\Captcha::widget([
                                'name'=>'Comment[captcha]',
                                'captchaAction'=>\Yii::$app->getModule('comments')->captchaAction,
                            ]);?>
                        </div>

                        <button class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="well">
    <div class="comment" data-id="0">
        <button class="btn btn-primary btn-lg comment-reply">Leave a Comment</button>
        <button class="btn btn-primary btn-lg comment-update">Update</button>
    </div>
    <div id="comments" class="comments">
        <?= $comments ?>
    </div>
</div>