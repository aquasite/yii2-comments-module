<?php

namespace terrasoff\yii2\comments\models;

use yii\base\Model;
use yii\base\Behavior;
use yii\base\ModelEvent;
use yii\db\ActiveRecord;

/**
 * Comment behaviot
 * @author terrasoff
 */
class CommentBehavior extends Behavior
{
    private $className = null;
    private $idAttribute = null;

    public function attach($owner) {
        parent::attach($owner);
        $this->className = get_class($owner);
        $this->idAttribute = $this->owner->getTableSchema()->primaryKey[0];
    }

    public function getClassName() {
        return $this->className;
    }

    public function getComments()
    {
        return $this->owner
            ->hasMany(Comment::className(), ['idClass' => $this->idAttribute])
            ->where(['className' => $this->className])
            ->orderBy('idParent', 'idComment');
    }

}